# to use this script
# . generate_input_list.sh input_dir N_files
# for example
#  . generate_input_list.sh  /home/weiy/transfer_server/user.weiy.900012.MGPy8EG_N30NLOA14_SMEFTNLO_gg2l2v_OtpSM_OpgSM.211002.cpG_cpt_ctp_rnd_100evt_EXT1 80

input_dir=$1
number_files=$2

find $input_dir -maxdepth 1 -type f -name '*.root' |head -$number_files| xargs readlink -f > input_list_test.txt
