rivet-mkhtml --mc-errs -o plots_EFT_operators \
YODA_inclusive_SM.yoda:'SM':'LineColor=black' \
YODA_inclusive_NPsq1.yoda:'$\textrm{c}_{\textrm{HG}}=$0.01, NP$^{\wedge}2==$1':'LineColor=blue' \
YODA_inclusive_NPsq2.yoda:'$\textrm{c}_{\textrm{HG}}=$0.01, NP$^{\wedge}2==$2':'LineColor={[cmyk]{0,51,53,0}}' \

