theApp.EvtMax = -1

import os, sys, subprocess
import AthenaPoolCnvSvc.ReadAthenaPool
#svcMgr.EventSelector.InputCollections = ["/eos/atlas/user/l/lxu/Workdir/VBS/VBS/Evgen/664429.MGPy8EvtGen_WWjj_lvqq_EW6_noTTbar/664429.evgen.root"]
#svcMgr.EventSelector.InputCollections = ["/afs/cern.ch/work/l/lxu/Analysis/VBS/Analysis/Rivet/Evgen/664429.evgen.root"]
svcMgr.EventSelector.InputCollections = []

input_list="input_list.txt"
if not os.path.isfile(input_list):
  print ("input list not found", input_list)
  sys.exit()

def copyAndUntarFile(infile=""):
  
  #copy file to localy dir and untar it
  
  newfiles=[]

  fname=infile.split('/')[-1]
  odir="/tmp/"+os.environ['USER']
  if not os.path.isdir(odir): os.makedirs(odir)
  # https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SPARatBNL#Access_to_the_datasets_on_BNL_dC
  if "/pnfs" in infile:
     cmd="xrdcp -f %s  %s" % (infile, odir) 
     print cmd
     os.system(cmd)
  elif "/eos" in infile:
     if "cern.ch" not in os.environ['HOST']:
       cmd="xrdcp %s  %s" % (infile, odir) 
       print cmd
       os.system(cmd)

  lfile=odir+"/"+fname
  if os.path.isfile(lfile):
    ## untar file 
    cmd="tar zxvf %s -C %s" % (lfile, odir)
    nfiles=subprocess.check_output(cmd.split())
    for nf in nfiles.split("\n"):
      ofile="%s/%s" % (odir, nf)
      if os.path.isfile(ofile):
        ## change file name
        ofile2="%s/%s_%s" % (odir, fname, nf)
        cmd="mv %s %s" % (ofile, ofile2)
        os.system(cmd)
        if os.path.isfile(ofile2):
          newfiles.append(ofile2)
  else:
    print "Error=> could not copy file", infile
    sys.exit()    

  print newfiles
  return newfiles


# to access dcache files at bnl
isBNL=False
myhost = os.uname()[1]
if "usatlas.bnl.gov" in myhost: isBNL=True
if "bnl.gov" in myhost: isBNL=True


with open(input_list, 'r') as fin:
  for lines in fin:
    line=lines.rstrip()
    if isBNL:
      if "/pnfs" in line: 
        #line =  "dcache:" + line
        line =  "root://dcgftp.usatlas.bnl.gov:1096/" + line
        # in case it's a tar file
        if "XYZ.tgz" in line:
          nlines=copyAndUntarFile(line)
          if nlines:
            for line in nlines:
              svcMgr.EventSelector.InputCollections.append(line)
        else:
          svcMgr.EventSelector.InputCollections.append(line)
      elif "/eos" in line: 
        line =  "root://eosatlas.cern.ch/" + line
        if "XYZ.tgz" in line:
          nlines=copyAndUntarFile(line)
          if nlines:
            for line in nlines:
              svcMgr.EventSelector.InputCollections.append(line)
        else:
          svcMgr.EventSelector.InputCollections.append(line)
      else:
        svcMgr.EventSelector.InputCollections.append(line)
    else:
      svcMgr.EventSelector.InputCollections.append(line)

#svcMgr.EventSelector.InputCollections.append('/home/weiy/transfer_server/user.weiy.900012.MGPy8EG_N30NLOA14_SMEFTNLO_gg2l2v_OtpSM_OpgSM.211002.cpG_cpt_ctp_rnd_100evt_EXT1/user.weiy.26830267.EXT1._000098.900012.evgen.root')
#svcMgr.EventSelector.InputCollections.append('/home/weiy/transfer_server/user.weiy.900012.MGPy8EG_N30NLOA14_SMEFTNLO_gg2l2v_OtpSM_OpgSM.211002.cpG_cpt_ctp_rnd_100evt_EXT1/user.weiy.26830267.EXT1._000099.900012.evgen.root')

###########################
### get syst weights
###########################
from PyUtils import AthFile
#print(svcMgr.EventSelector.InputCollections[0])
#print(svcMgr.EventSelector.InputCollections)

af = AthFile.fopen(svcMgr.EventSelector.InputCollections[0]) #opens the first file from the InputCollections list
af.fileinfos #this is a dict of dicts, take a look at what's available (e.g. do af.fileinfos.keys() to see the main keys)!
#Below are some example
systWeights=None

def safeFileName(name):
 name=name.strip()
 name= name.replace(".","p").replace(" ","_")
 name= name.replace("pyoda",".yoda")
 name= name.replace(":","_")
 name= name.replace("/","_")
 return name

metadata = af.fileinfos['metadata']
if '/Generation/Parameters' in metadata:
    genpars=metadata['/Generation/Parameters']
    if 'HepMCWeightNames' in genpars:
        systWeights=genpars['HepMCWeightNames']
    else:
        print 'HepMCWeightName not found in /Generation/Parameters:'
        print genpars
        #raise RuntimeError('HepMCWeightName not found in /Generation/Parameters. Exiting...')
else:
    print '/Generation/Parameters not found in metadata:'
    print metadata
    #raise RuntimeError('/Generation/Parameters not found in metadata. Exiting...')

###########################

from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from GaudiSvc.GaudiSvcConf import THistSvc
ServiceMgr += THistSvc()
ServiceMgr.THistSvc.Output = ["Rivet DATAFILE='.root' OPT='RECREATE'"]

from Rivet_i.Rivet_iConf import Rivet_i

# loop all event weights
analyses= "ATLAS_2019_llvv"

nominalWtNames=["Default", "Weight"] ## nominal name for Rel21

doSyst=0

if systWeights==None: systWeights={'Default': 0}
nwt=0
for i in systWeights:

    systName=safeFileName(i)

    print 'weight name:',i,', output name',systName
    if (not i) and (not systName): systName="nominal" ## special case
    print "systName=", systName

    ## do nominal only. NEED TO KNOW WHICH ONE IS NOMINAL!
    if not doSyst:
      isNom=0
      for twt in nominalWtNames:
        if i.replace(' ', '', 1000)==twt:
          isNom=1
          break
      if not isNom: continue 

    ServiceMgr.THistSvc.Output = ["Rivet DATAFILE='_"+systName+".root' OPT='RECREATE'"]

    rivet = Rivet_i(systName)
    rivet.AnalysisPath = os.environ['PWD']
    for analysis in analyses.split(","):
      rivet.Analyses +=[analysis]
  
    rivet.RunName = ""
    if i!="Default" : rivet.WeightName=i
    rivet.HistoFile = "_"+systName
    if doSyst:
      rivet.DoRootHistos = False  ## TODO: it looks Rivet could not save root files when running multiple weights?
    else:
      rivet.DoRootHistos = True
    #rivet.CrossSection = 1

    job += rivet

    #if nwt>2: break ## test only
