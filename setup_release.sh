export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh

# asetup 21.6, latest, AthGeneration
asetup 21.6, 21.6.84, AthGeneration
#asetup 21.6, 21.6.84, AthGeneration #the version I used, corresponding to Rivet version 3.1.4

source setupRivet.sh
