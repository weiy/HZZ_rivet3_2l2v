theApp.EvtMax = -1
#include("GeneratorUtils/StdAnalysisSetup.py")

import os
import AthenaPoolCnvSvc.ReadAthenaPool

from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i()
rivet.AnalysisPath = os.environ['PWD']

# rivet.Analyses += [ 'ATLAS_2021_I1849535' ]
# rivet.Analyses += [ 'ATLAS_2020_I1790439' ]
rivet.Analyses += [ 'ATLAS_2019_llvv' ]
rivet.RunName = ''
rivet.HistoFile = 'MyOutput.yoda.gz'
rivet.CrossSection = 1.0
# rivet.IgnoreBeamCheck = 1
#The cross-section is deliberately set to 1.0 as the merging scripts makes it very straightforward to scale the output files to some cross-section later on.
job += rivet

# import time
# time.sleep(1200)
