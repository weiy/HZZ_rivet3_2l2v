// rivet2 to rivet3: not complete
// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/DressedLeptons.hh"
//#include "Rivet/Tools/Cutflow.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/VisibleFinalState.hh"
#include "Rivet/Projections/InvMassFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"
#include <Rivet/Event.hh>

namespace Rivet {

  class ATLAS_2019_llvv : public Analysis {
    public:

      /// Constructor
      DEFAULT_RIVET_ANALYSIS_CTOR(ATLAS_2019_llvv);

      void init() {
        // Eta ranges
        Cut eta_full = (Cuts::abseta < 4.2) & (Cuts::pT >= 1.0*MeV);
        Cut lep_cuts = (Cuts::abseta < 2.5) && (Cuts::pT > 10*GeV);
        // All final state particles
        FinalState fs(eta_full);

        // *** ELECTRON ***
        // Get photons to dress leptons
        IdentifiedFinalState photons(fs);
        photons.acceptIdPair(PID::PHOTON);

        // Projection to find the electrons
        IdentifiedFinalState el_id(fs);
        el_id.acceptIdPair(PID::ELECTRON);

        PromptFinalState electrons(el_id);
	    electrons.acceptTauDecays(false);
        declare(electrons, "electrons");

        DressedLeptons dressedelectrons(photons, electrons, 0.1, lep_cuts, true, true);
        declare(dressedelectrons, "dressedelectrons");

        //electrons for eoverlap removal
        DressedLeptons ewdressedelectrons(photons, electrons, 0.1, eta_full, true, true);
        declare(ewdressedelectrons, "ewdressedelectrons");               

        // *** MUONS ***
        // Projection to find the muons
        IdentifiedFinalState mu_id(fs);
        mu_id.acceptIdPair(PID::MUON);

        PromptFinalState muons(mu_id);
        muons.acceptTauDecays(false);
        declare(muons, "muons");

        DressedLeptons dressedmuons(photons, muons, 0.1, lep_cuts, true, true);
        declare(dressedmuons, "dressedmuons");
       	
        // muons for jet overlap removal
        DressedLeptons ewdressedmuons(photons, muons, 0.1, eta_full, true, true);
        declare(ewdressedmuons, "ewdressedmuons");
 
        // *** NEUTRINOS ***
        // Access all neutrinos
        IdentifiedFinalState nu_all;
        nu_all.acceptNeutrinos();
        PromptFinalState nu(nu_all);
        nu.acceptTauDecays(false);
        declare(nu, "nu");

        // Projection to find neutrinos
        IdentifiedFinalState nu_id;
        nu_id.acceptNeutrinos();
        PromptFinalState neutrinos(nu_id);
        neutrinos.acceptTauDecays(false);

        // *** MET ***
        VetoedFinalState ivfs(fs);
        ivfs.addVetoOnThisFinalState(VisibleFinalState(fs));
        declare(ivfs, "invisible");

        // Jet clustering.
        VetoedFinalState vfs;
        vfs.addVetoOnThisFinalState(ewdressedelectrons);
        vfs.addVetoOnThisFinalState(ewdressedmuons);
        vfs.addVetoOnThisFinalState(neutrinos);

        FastJets jets(vfs, FastJets::ANTIKT, 0.4, JetAlg::Muons::ALL, JetAlg::Invisibles::NONE);
        jets.useInvisibles(JetAlg::Invisibles::ALL);
        declare(jets, "jets");

        // Histogram booking
        //_h_e_l_pt                       = bookHisto1D("SM_e_l_pt", 40, 0, 350);
        book(_h["_h_e_l_pt"], "SM_e_l_pt", 40, 0, 350);
        book(_h["_h_e_sl_pt"], "SM_e_sl_pt", 40, 0, 350);
        book(_h["_h_nJets_ee"], "Jet_Multiplicity_ee",4,-0.5,4.5);
        book(_h["_h_met_ee"], "Missing_ET_ee",50,75.5,700.5);
        book(_h["_h_Z1_pT_ee"], "SM_Z1_pT_ee",40,0,500);
        book(_h["_h_Z1_mass_ee"], "SM_Z1_mass_ee",60,70,120);
        book(_h["_h_Z2vv_pT_ee"], "SM_Z2vv_pT_ee",40,50,600);
        book(_h["_h_Z2vv_mass_ee"], "SM_Z2vv_mass_ee",60,70,120);
        book(_h["_h_mTZZ_eevv"], "SM_mTZZ_eevv", 40, 100, 1500);
        book(_h["_h_dPhiZMET_ee"], "SM_dPhiZMET_ee", 40, 2.7, 3.2);
        book(_h["_h_HpT_ee"], "SM_HpT_ee", 60, 0, 30);
        book(_h["_h_Hmass_ee"], "SM_Hmass_ee", 60, 99.5, 1999.5);
        //book(_h["_h_Hmass_ee"], "SM_Hmass_ee", 30, 100, 150);
        book(_h["_h_dR_ee"], "SM_dR_ee", 20, 0, 4.2);

        book(_h["_h_m_l_pt"], "SM_m_l_pt", 40, 0, 350);
        book(_h["_h_m_sl_pt"], "SM_m_sl_pt", 40, 0, 350);
        book(_h["_h_nJets_mm"], "Jet_Multiplicity_mm",4,-0.5,4.5);
    	book(_h["_h_met_mm"], "Missing_ET_mm",50,75.5,700.5);
        book(_h["_h_Z1_pT_mm"], "SM_Z1_pT_mm",40,0,500);
        book(_h["_h_Z1_mass_mm"], "SM_Z1_mass_mm",60,70,120);
        book(_h["_h_Z2vv_pT_mm"], "SM_Z2vv_pT_mm",40,50,600);
        book(_h["_h_Z2vv_mass_mm"], "SM_Z2vv_mass_mm",60,70,120);
        book(_h["_h_mTZZ_mmvv"], "SM_mTZZ_mmvv", 40, 100, 1500);
        book(_h["_h_dPhiZMET_mm"], "SM_dPhiZMET_mm", 40, 2.7, 3.2);
        book(_h["_h_HpT_mm"], "SM_HpT_mm", 60, 0, 30);
        book(_h["_h_Hmass_mm"], "SM_Hmass_mm", 60, 99.5, 1999.5);
        book(_h["_h_dR_mm"], "SM_dR_mm", 20, 0, 4.2);

        book(_h["_h_cutflow"], "ZZllvv_Cutflow",4,0,4);
      }

      void analyze(const Event& event) {

        //const double weight = event.weight();
        //const double weight = 1;
        _h["_h_cutflow"]->fill(0, 1.0); //Total number of events

        ////////////////////////////////////////////////////////////////////
        // Defining leptons/neutrinos for ZZ->llvv final state
        ////////////////////////////////////////////////////////////////////
        vector<DressedLepton> electrons = sortByPt(applyProjection<DressedLeptons>(event, "dressedelectrons").dressedLeptons());
        vector<DressedLepton> muons     = sortByPt(applyProjection<DressedLeptons>(event, "dressedmuons").dressedLeptons());
	
        const Jets& jets = sortByPt(applyProjection<FastJets>(event, "jets").jetsByPt(Cuts::pT > 20*GeV && Cuts::abseta < 4.5));
        const FinalState& invisible = apply<VetoedFinalState>(event, "invisible");
        const FinalState& neutrinos = applyProjection<FinalState>(event, "nu");

        // Calculate MET
        const FourMomentum met = sum(invisible.particles(), FourMomentum());

        FourMomentum nu_electron_0;
        FourMomentum nu_electron_1;
        FourMomentum nu_ee_0;
        FourMomentum nu_ee_1;
        FourMomentum nu_mm_0;
        FourMomentum nu_mm_1;
        FourMomentum dilep_ee;
        FourMomentum dinu_ee;
        FourMomentum dinu_mm;
        FourMomentum dilep_mm;
        FourMomentum Hcandidate_ee;
        FourMomentum Hcandidate_mm;
        double Zmass = 91.1876; // [GeV]

        /////////////////////////////////////////////////////////////////////////////
        //			 Event selection 2 leptons  
        /////////////////////////////////////////////////////////////////////////////
        if((electrons.size()+muons.size())!=2){vetoEvent;}
        _h["_h_cutflow"]->fill(1, 1.0);	
          
        /////////////////////////////////////////////////////////////////////////////
        // 		 	Event selection H->ZZ->2e2v - SFOS:same flavour opposite sign  
        /////////////////////////////////////////////////////////////////////////////
	
        int met_cut = 80;

        if(electrons.size() == 2 && muons.empty() && met.pT()>met_cut && neutrinos.size() == 2)  {
            double charge1 = electrons.at(0).constituentLepton().charge();
            double charge2 = electrons.at(1).constituentLepton().charge();
            if((charge1*charge2)>0){vetoEvent;}

            for (const Jet& j: jets){
              for (const Particle& e: electrons){
                const double dR_ejet = deltaR(e.momentum(), j.momentum());
                if (dR_ejet < 0.2 && e.abspid() == PID::ELECTRON){vetoEvent;}
              }
            }

            dilep_ee = electrons.at(0).momentum()+electrons.at(1).momentum();
            nu_ee_0 = neutrinos.particlesByPt()[0].momentum();
            nu_ee_1 = neutrinos.particlesByPt()[1].momentum();
            dinu_ee = nu_ee_0 + nu_ee_1;
            double eTmiss_ee = met.pT();
            double dR_ee = deltaR(electrons.at(0).momentum(), electrons.at(1).momentum());
            double mT_ee = sqrt(pow(sqrt(pow(Zmass,2)+pow(dilep_ee.pT(),2))+sqrt(pow(Zmass,2)+pow(eTmiss_ee,2)),2) - (pow(dilep_ee.px()+met.px(),2)+pow(dilep_ee.py()+met.py(),2)));
            double dPhiZMET_ee = fabs(deltaPhi(dilep_ee, met));
            if((dinu_ee.pT()<met_cut)){vetoEvent;}
            Hcandidate_ee = dinu_ee + dilep_ee;
            double H_mass_ee = Hcandidate_ee.mass();

            //if (H_mass_ee > 130) {
            _h["_h_e_l_pt"]->fill(electrons.at(0).pT());
            _h["_h_e_sl_pt"]->fill(electrons.at(1).pT());
            _h["_h_Z1_pT_ee"]->fill(dilep_ee.pT());
            _h["_h_Z1_mass_ee"]->fill(dilep_ee.mass());
            _h["_h_met_ee"]->fill(eTmiss_ee);
            _h["_h_nJets_ee"]->fill(jets.size());	
            _h["_h_dR_ee"]->fill(dR_ee);
            _h["_h_mTZZ_eevv"]->fill(mT_ee);
            _h["_h_dPhiZMET_ee"]->fill(dPhiZMET_ee);
            _h["_h_Hmass_ee"]->fill(H_mass_ee);
            _h["_h_HpT_ee"]->fill(Hcandidate_ee.pT());
            _h["_h_Z2vv_pT_ee"]->fill(dinu_ee.pT());
            _h["_h_Z2vv_mass_ee"]->fill(dinu_ee.mass());
            _h["_h_cutflow"]->fill(2, 1.0);
            //}
        }
        /////////////////////////////////////////////////////////////////////////////
        //                     Event selection H->ZZ->2u2v - SFOS  
        /////////////////////////////////////////////////////////////////////////////
 
        if(muons.size() == 2 && electrons.empty() && met.pT()>met_cut && neutrinos.size() == 2) {
		    double charge1 = muons.at(0).constituentLepton().charge();
            double charge2 = muons.at(1).constituentLepton().charge();
            if((charge1*charge2)>0){vetoEvent;}

            for (const Jet& j: jets){
              for (const Particle& m: muons){
                const double dR_mjet = deltaR(m.momentum(), j.momentum());
                if (dR_mjet < 0.2 && m.abspid() == PID::MUON){vetoEvent;}
              }
            }

            dilep_mm = muons.at(0).momentum()+muons.at(1).momentum();
            nu_mm_0 = neutrinos.particlesByPt()[0].momentum();
            nu_mm_1 = neutrinos.particlesByPt()[1].momentum();
            dinu_mm = nu_mm_0 + nu_mm_1;
            double eTmiss_mm = met.pT();
            double dR_mm = deltaR(muons.at(0).momentum(), muons.at(1).momentum()); 
            double mT_mm = sqrt(pow(sqrt(pow(Zmass,2)+pow(dilep_mm.pT()/GeV,2))+sqrt(pow(Zmass,2)+pow(eTmiss_mm/GeV,2)),2) - (pow(dilep_mm.px()/GeV+met.px()/GeV,2)+pow(dilep_mm.py()/GeV+met.py()/GeV,2)));
            double dPhiZMET_mm = fabs(deltaPhi(dilep_mm, met));
            if((dinu_mm.pT()<met_cut)){vetoEvent;}
            Hcandidate_mm = dinu_mm + dilep_mm;
            double H_mass_mm = Hcandidate_mm.mass();
               
            //if (H_mass_mm > 130) { 
              _h["_h_m_l_pt"]->fill(muons.at(0).pT());
              _h["_h_m_sl_pt"]->fill(muons.at(1).pT());
              _h["_h_Z1_pT_mm"]->fill(dilep_mm.pT());
              _h["_h_Z1_mass_mm"]->fill(dilep_mm.mass());
              _h["_h_met_mm"]->fill(eTmiss_mm);	
              _h["_h_nJets_mm"]->fill(jets.size());
              _h["_h_dR_mm"]->fill(dR_mm);
              _h["_h_mTZZ_mmvv"]->fill(mT_mm);
              _h["_h_dPhiZMET_mm"]->fill(dPhiZMET_mm);
              _h["_h_Hmass_mm"]->fill(H_mass_mm);
              _h["_h_HpT_mm"]->fill(Hcandidate_mm.pT());
              _h["_h_Z2vv_pT_mm"]->fill(dinu_mm.pT());
              _h["_h_Z2vv_mass_mm"]->fill(dinu_mm.mass());	
              _h["_h_cutflow"]->fill(3, 1.0);
              //}
        }
	
      } 

      void finalize() {

        double lumi = 139; // [fb^-1]
        //double N_generated = numEvents();
	
        //double xsec = crossSection()/femtobarn;

        //double sf = (xsec*lumi)/N_generated;
        //const double sf = lumi * crossSection() / femtobarn / sumW();
        const double sf = lumi * crossSectionPerEvent() / femtobarn;
        //const double sf = lumi * crossSection() / femtobarn / sumOfWeights();

        cout<<"weiy: sf:"<<sf<<endl;
        //sf=1390
	
        //cout<<"weiy: crossSection():"<<crossSection()<<endl;
        //cross-section: -0.000950034

        //cout<<"weiy: femtobarn:"<<femtobarn<<endl;
        //femtobarn=0.001
	
        cout<<"weiy: sumW:"<<sumW()<<endl;
        //cout<<"weiy: sumOfWeights:"<<sumOfWeights()<<endl;
        //sumW()=sumOfWeights()
        //sumW()=-0.0950034 ctp
        //cpG0.950034

        //for (auto hist : _h) { scale(hist.second, sf); }
        scale(_h, sf);
        /*
          scale(_h_Z1_pT_ee,sf);
          scale(_h_Z1_pT_mm    ,sf);
          scale(_h_Z1_mass_ee  ,sf);
          scale(_h_Z1_mass_mm  ,sf);
          scale(_h_Z2vv_pT_ee  ,sf);
          scale(_h_Z2vv_mass_ee,sf);
          scale(_h_Z2vv_pT_mm  ,sf);
          scale(_h_Z2vv_mass_mm,sf);
          scale(_h_e_l_pt      ,sf);
          scale(_h_e_sl_pt     ,sf);
          scale(_h_m_l_pt      ,sf);
          scale(_h_m_sl_pt     ,sf);
          scale(_h_met_ee      ,sf);
          scale(_h_met_mm      ,sf);
          scale(_h_nJets_ee    ,sf);
          scale(_h_nJets_mm    ,sf);
          scale(_h_mTZZ_eevv   ,sf);
          scale(_h_mTZZ_mmvv   ,sf);
          scale(_h_dPhiZMET_ee ,sf);
          scale(_h_dPhiZMET_mm ,sf);
          scale(_h_HpT_ee      ,sf);
          scale(_h_Hmass_ee    ,sf);
          scale(_h_dR_ee       ,sf);
          scale(_h_HpT_mm      ,sf);
          scale(_h_Hmass_mm    ,sf);
          scale(_h_dR_mm       ,sf);
        */
	
        /*_h_Z1_pT_ee     ->normalize(1.0);
          _h_Z1_pT_mm     ->normalize(1.0);
          _h_Z1_mass_ee   ->normalize(1.0);
          _h_Z1_mass_mm   ->normalize(1.0);
          _h_Z2vv_pT_ee   ->normalize(1.0);
          _h_Z2vv_mass_ee ->normalize(1.0);
          _h_Z2vv_pT_mm   ->normalize(1.0);
          _h_Z2vv_mass_mm ->normalize(1.0);
          _h_e_l_pt       ->normalize(1.0);
          _h_e_sl_pt      ->normalize(1.0);
          _h_m_l_pt       ->normalize(1.0);
          _h_m_sl_pt      ->normalize(1.0);
          _h_met_ee       ->normalize(1.0);
          _h_met_mm       ->normalize(1.0);
          _h_nJets_ee     ->normalize(1.0);
          _h_nJets_mm     ->normalize(1.0);
          _h_mTZZ_eevv    ->normalize(1.0);
          _h_mTZZ_mmvv    ->normalize(1.0);
          _h_dPhiZMET_ee  ->normalize(1.0);
          _h_dPhiZMET_mm  ->normalize(1.0);
          _h_HpT_ee       ->normalize(1.0);
          _h_Hmass_ee     ->normalize(1.0);
          _h_dR_ee        ->normalize(1.0);
          _h_HpT_mm       ->normalize(1.0);
          _h_Hmass_mm     ->normalize(1.0);
          _h_dR_mm        ->normalize(1.0);*/

      }

    map<string, Histo1DPtr> _h;
    
    private:
      Histo1DPtr _h_cutflow;
      Histo1DPtr _h_Z1_pT_ee;
      Histo1DPtr _h_Z1_pT_mm;
      Histo1DPtr _h_Z1_mass_ee;
      Histo1DPtr _h_Z1_mass_mm;
      Histo1DPtr _h_Z2vv_pT_ee;
      Histo1DPtr _h_Z2vv_mass_ee;
      Histo1DPtr _h_Z2vv_pT_mm;
      Histo1DPtr _h_Z2vv_mass_mm;
      Histo1DPtr _h_e_l_pt;
      Histo1DPtr _h_e_sl_pt;
      Histo1DPtr _h_m_l_pt;
      Histo1DPtr _h_m_sl_pt;
      Histo1DPtr _h_met_ee;
      Histo1DPtr _h_met_mm;
      Histo1DPtr _h_nJets_ee;
      Histo1DPtr _h_nJets_mm;
      Histo1DPtr _h_mTZZ_eevv;
      Histo1DPtr _h_mTZZ_mmvv;
      Histo1DPtr _h_dPhiZMET_ee;
      Histo1DPtr _h_dPhiZMET_mm;
      Histo1DPtr _h_HpT_ee;
      Histo1DPtr _h_Hmass_ee;
      Histo1DPtr _h_dR_ee;
      Histo1DPtr _h_HpT_mm;
      Histo1DPtr _h_Hmass_mm;
      Histo1DPtr _h_dR_mm;
   }; 

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ATLAS_2019_llvv);

}
