#####local#######

# tmux
# . generate_input_list.sh input_dir N_files #generate input EVNT files
# source setup_release.sh                    #source setup
# source Compile.sh                          #compile rivet routine
# source run_temp_EFT2l2v.sh &               #run
# tmux detach


#####grid#######
rm -rf .git

source setup_release.sh

source Compile.sh

#source run_rivet4l.sh
cat ${HOME}/.voms_password | voms-proxy-init --voms atlas
lsetup panda

# the nFilesPerJob is very important.


inputdir=user.weiy.900021.MGPy8EG_N30NLOA14_SMEFTNLO_gg2l2v_SM.230508.cpG_cpt_ctp1_EXT1/
outputdir=user.${USER}.900021.MGPy8EG_N30NLOA14_SMEFTNLO_gg2l2v_SM.rivetZZ.xs1_4

# inputdir=mc21_13p6TeV.700679.Sh_2212_NNPDF30NNLO_CKKW15_ZZ.evgen.EVNT.e8481/
# outputdir=user.weiy.mc21_13p6TeV.700679.CKKW15_ZZ.rivetZZ22.6.xs1_m4l_sleep

# inputdir=mc21_13p6TeV.700680.Sh_2212_NNPDF30NNLO_CKKW30_ZZ.evgen.EVNT.e8481/
# outputdir=user.weiy.mc21_13p6TeV.700680.CKKW30_ZZ.rivetZZ22.6.xs1_m4l_sleep

# inputdir=mc21_13p6TeV.700681.Sh_2212_NNPDF30NNLO_CSSKIN_ZZ.evgen.EVNT.e8481/
# outputdir=user.weiy.mc21_13p6TeV.700681.CSSKIN_ZZ.rivetZZ22.6.xs1_m4l_sleep

# inputdir=mc21_13p6TeV.700682.Sh_2212_NNPDF30NNLO_QSF025_ZZ.evgen.EVNT.e8481/
# outputdir=user.weiy.mc21_13p6TeV.700682.QSF025_ZZ.rivetZZ22.6.xs1_m4l_sleep

# inputdir=mc21_13p6TeV.700683.Sh_2212_NNPDF30NNLO_QSF4_ZZ.evgen.EVNT.e8481/
# outputdir=user.weiy.mc21_13p6TeV.700683.QSF4_ZZ.rivetZZ22.6.xs1_m4l_sleep


extFile=RivetATLAS_2019_llvv.so,ATLAS_2019_llvv.plot,ATLAS_2019_llvv.cc

official="false"

if [[ ${official} == "false" ]]; then
    echo "submit to personal grid..."
    pathena --nFilesPerJob=100 \
            --extOutFile=MyOutput.yoda* \
            --inDS=${inputdir} \
            --outDS=${outputdir} \
            --forceStaged \
            --disableAutoRetry \
            --extFile=${extFile} \
            grid_JO.py

else
    echo "submit to group grid..."
    pathena \
        --official \
        --voms=atlas:/atlas/phys-higgs/Role=production \
        --nFilesPerJob=6 \
        --extOutFile=MyOutput.yoda* \
        --inDS=${inputdir} \
        --noBuild \
        --noBuild \
        --forceStaged \
        --disableAutoRetry \
        --outDS=group.phys-higgs.${outputdir} \
        --extFile=${extFile} \
        grid_JO.py
fi
