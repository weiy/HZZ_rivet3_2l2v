#!/bin/bash

JO=JO_RivetAnalysis_temp_rel21.py  ## no weights
#JO=JO_RivetAnalysis_temp_syst_rel21.py  ## with weights

# file list
inputs=input_list_test.txt

# Rivet analysis
ana="ATLAS_2019_llvv"
lib=RivetATLAS_2019_llvv.so # RivetAnalysis.so

# output dir
outdir=output_EFT

echo "point1"

## Run Rivet

if [ ! -d ${outdir} ]; then
  mkdir -p ${outdir}
fi

JO=`readlink -f ${JO}`
inputs=`readlink -f ${inputs}`
lib=`readlink -f ${lib}`

thisdir=`pwd`
cd ${outdir}

cp ${JO} .
## replace some placeholders
JOname=`basename ${JO}`
JOdir=`dirname ${JO}`
sed -i "s/RIVETANALYSIS/${ana}/g" ${JOname}
sed -i "s/RIVETOUTPUT/${outname}/g" ${JOname}

echo "point2"

cp ${lib} .
for file in `ls ${JOdir}/${ana}.* `; do
  lfile=`readlink -f ${file}`
  ln -s ${lfile}
done
cp ${inputs} input_list.txt

###################
## run local job
###################
echo "point3"

pwd

athena ${JOname} >& log_rivet

echo "point4"

cd ${thisdir}

#done
