theApp.EvtMax = -1

import AthenaPoolCnvSvc.ReadAthenaPool
# svcMgr.EventSelector.InputCollections = [ 'EVNT.root' ]
svcMgr.EventSelector.InputCollections = []

input_list="input_list.txt"
if not os.path.isfile(input_list):
  print("input list not found", input_list)
  sys.exit()

with open(input_list, 'r') as fin:
    for lines in fin:
        line=lines.rstrip()
        svcMgr.EventSelector.InputCollections.append(line)

from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i()
import os
rivet.AnalysisPath = os.environ['PWD']

rivet.Analyses += [ 'ATLAS_2019_llvv' ]
rivet.RunName = ''
rivet.HistoFile = 'MyOutput.yoda.gz'
rivet.CrossSection = 1.0
#rivet.IgnoreBeamCheck = True
#rivet.SkipWeights=True
job += rivet
