# This is a project on checking HZZ 2l2v truth sample by rivet 


## __Rivet routine instructions__

Example Rivet analysis code to process __EVNT__ files. 
To get started:
Clone the repo: `git clone https://gitlab.cern.ch/weiy/HZZ_rivet3_2l2v.git`

Cd into the directory: `cd HZZ_rivet3_2l2v`
All the following commands are run inside this main directory. 

### __A wrapper__
A wrapper file __command.txt__ is provided. Go to check each step and run one by one at first to check.

### __Generate input file list__
__generate_input_list.sh__ is used to generate an input file list. Check the script to get to know how to use it.

### __Set-up__
To set-up the environment run:
`source setup_release.sh`

Should note that we will use rivet3 now.

### __Compile__
To compile the source code:
`source Compile.sh`

### __Analysis code__
Inside the folder you'll see there's a .cc and a .plot file. The .cc file is the main analysis code, where you have three key functions: `init()`, `analyze()` and `finalize()`. The `init()` function is called once at the start, and is basically used to defined the objects and histograms. The `analyze()` function is the main event loop where you apply the event selection and fill the histograms. The `finalize()` function is used to scale the histgrams as desired. Note in this example script I have left uncommented how you would normalise to 139 fb^-1. Commented out underneath is how you would normalise to 1 for a shape comparison.

### __Run the code__
`source run_temp_EFT2l2v.sh &`


## __Rivet plot__

The .plot file is where you set different properties for the plots, i.e. the axis labels, axis ranges, if you want the y-axis to be log scale, etc. You can even choose the Y-axis range of the ratio plot using `RatioPlotYMax` and `RatioPlotYMin`.
Note the title displayed in the legend for the plots is set in the [__plot.sh__](plot.sh) file. 

If you want to visualise these plots, you can then run: `rivet-mkhtml --mc-errs -o plots_EFT YODA_inclusive_SM.yoda:'Title=NAME':'LineColor=blue'`
All the plots will appear in a folder titled `plots_VBF_comparison`. If you scp this folder to your local computer, open it, and double click on the `index.html` file, you should see all the plots appear in your web browser.

If you want to have a ratio plot - you'll of course need another file to compare to, so again run the command above, changing the input EVNT filename, and the output .yoda file name (e.g. to `YODA_old_samples.yoda`)
Then to convert to .yoda files to actual plots, run: `source plot.sh`. Or see __plot_command.txt__ to see more details.

__NOTE:__ when plot different weights: `Variations=none`, `DefaultWeight=cpG_10_lin` should be double checked and make sure it's clear.

More instructions on plotting can be found here: [https://gitlab.com/hepcedar/rivet/-/tree/release-3-1-x/doc/tutorials](https://gitlab.com/hepcedar/rivet/-/tree/release-3-1-x/doc/tutorials)
