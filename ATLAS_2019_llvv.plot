## BEGIN PLOT /ATLAS_2019_llvv/*
LegendXPos=0.53
LegendYPos=0.93
RatioPlotYMax=50
RatioPlotYMin=-10
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/ZZllvv_Cutflow
XLabel=$ZZ\rightarrow \ell\ell\nu\nu$ Cutflow
YLabel=Raw events
Title= Cutflow: 0=tot., 1=2$e/2\mu$, 2=2$e2\nu$ SR, 3=2$\mu2\nu$ SR
LogY=0
#RatioPlotYMax=2
#RatioPlotYMin=-20
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/Missing_ET_ee
XLabel=$E^{\textrm{miss}}_T$ [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title=Missing transverse energy ($ee$ channel)
LogY=0
#YMax = 0
#YMin = -0.03
#RatioPlotYMax=10
#RatioPlotYMin=-10
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/Missing_ET_mm
XLabel=$E^{\textrm{miss}}_T$ [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title=Missing transverse energy ($\mu\mu$ channel)
LogY=0
#YMax = 0
#YMin = -0.03
#RatioPlotYMax=10
#RatioPlotYMin=-10
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_Z1_mass_ee
XLabel=$m_{\ell\ell}$ [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title= Invariant mass of Z ($ee$ channel)
LogY=0
#RatioPlotYMax=15
#RatioPlotYMin=-15
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_Z1_mass_mm
XLabel=$m_{\ell\ell}$ [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title= Invariant mass of Z ($\mu\mu$ channel)
LogY=0
#RatioPlotYMax=15
#RatioPlotYMin=-15
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_Z1_pT_ee
XLabel=$p_{T}$ ($Z_{1}$) [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title= pT of Z ($ee$ channel)
LogY=0
#RatioPlotYMax=15
#RatioPlotYMin=-15
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_Z1_pT_mm
XLabel=$p_{T}$ ($Z_{1}$) [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title= pT of Z ($\mu\mu$ channel)
LogY=0
#RatioPlotYMax=15
#RatioPlotYMin=-15
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_Z2vv_mass_ee
XLabel=$m_{\nu\nu}$ [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title= Invariant mass of Z ($\nu\nu$ channel) where other $Z\rightarrow ee$
LogY=0
#RatioPlotYMax=15
#RatioPlotYMin=-15
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_Z2vv_mass_mm
XLabel=$m_{\nu\nu}$ [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title= Invariant mass of Z ($\nu\nu$ channel) where other $Z\rightarrow \mu\mu$
LogY=0
#RatioPlotYMax=15
#RatioPlotYMin=-15
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_Z2vv_pT_ee
XLabel=$p_{T}$ ($Z_{2}$) [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title= pT of Z ($\nu\nu$ channel) where other $Z\rightarrow ee$
LogY=0
#YMax = 25
#RatioPlotYMax=10
#RatioPlotYMin=-10
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_Z2vv_pT_mm
XLabel=$p_{T}$ ($Z_{2}$) [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title= pT of Z ($\nu\nu$ channel) where other $Z\rightarrow \mu\mu$
LogY=0
#YMax= 5
#RatioPlotYMax=10
#RatioPlotYMin=-10
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_e_l_pt
XLabel=$p_{T}$ ($e_{1}$) [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title= pT of leading $e$
LogY=0
#RatioPlotYMax=10
#RatioPlotYMin=-10
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_e_sl_pt
XLabel=$p_{T}$ ($e_{2}$) [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title= pT of sub-leading $e$
LogY=0
#RatioPlotYMax=10
#RatioPlotYMin=-10
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_m_l_pt
XLabel=$p_{T}$ ($\mu_{1}$) [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title= pT of leading $m$ 
LogY=0
#RatioPlotYMax=10
#RatioPlotYMin=-10
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_m_sl_pt
XLabel=$p_{T}$ ($\mu_{2}$) [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title= pT of sub-leading $m$    
LogY=0
#RatioPlotYMax=10
#RatioPlotYMin=-10
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/Jet_Multiplicity_ee
XLabel = Number of jets in the event
YLabel=$\sigma \cdot \mathcal{L}$
Title = nJets ($ee$ channel)
LogY=0
#RatioPlotYMax=10
#RatioPlotYMin=-10
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/Jet_Multiplicity_mm
XLabel = Number of jets in the event
YLabel=$\sigma \cdot \mathcal{L}$
Title = nJets ($\mu\mu$ channel)
LogY=0
#RatioPlotYMax=10
#RatioPlotYMin=-10
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_HpT_ee
XLabel=$p_{T}^{ZZ}$ [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title= pT of Higgs candidate ($H\rightarrow ZZ \rightarrow ee\nu\nu$ channel)
LogY=0
#YMax = 70
#RatioPlotYMax=15
#RatioPlotYMin=-15
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_HpT_mm
XLabel=$p_{T}^{ZZ}$ [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title= pT of Higgs candidate ($H\rightarrow ZZ \rightarrow \mu\mu\nu\nu$ channel)
LogY=0
#YMax = 75
#RatioPlotYMax=15
#RatioPlotYMin=-15
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_Hmass_ee
XLabel=$m_{ZZ}$ [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title= Mass of Higgs candidate ($H\rightarrow ZZ \rightarrow ee\nu\nu$ channel)
LogY=0
#YMax = 0
#YMin = -0.1
#RatioPlotYMax=15
#RatioPlotYMin=-15
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_Hmass_mm
XLabel=$m_{ZZ}$ [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title= Mass of Higgs candidate ($H\rightarrow ZZ \rightarrow \mu\mu\nu\nu$ channel)
LogY=0
#YMax = 0
#YMin = -0.1
#RatioPlotYMax=15
#RatioPlotYMin=-15
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_dPhiZMET_ee
XLabel=$\Delta\phi(Z, E^{\textrm{miss}}_{T})$ [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title=$\Delta\phi$ between Z and MET ($ee$ channel)
LogY=0
#RatioPlotYMax=20
#RatioPlotYMin=-20
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_dPhiZMET_mm
XLabel=$\Delta\phi(Z, E^{\textrm{miss}}_{T})$ [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title=$\Delta\phi$ between Z and MET ($\mu\mu$ channel)
LogY=0
#RatioPlotYMax=20
#RatioPlotYMin=-20
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_mTZZ_eevv
XLabel=$m_{T}^{ZZ}$ [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title=Transverse mass ZZ distribution ($ee$ channel)
LogY=0
#YMax = 4
#RatioPlotYMax=10
#RatioPlotYMin=-10
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_mTZZ_mmvv
XLabel=$m_{T}^{ZZ}$ [GeV]
YLabel=$\sigma \cdot \mathcal{L}$
Title=Transverse mass ZZ distribution ($\mu\mu$ channel)
LogY=0
#YMax = 4
#RatioPlotYMax=10
#RatioPlotYMin=-10
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_dR_ee
XLabel=$\Delta R_{ll}$
YLabel=$\sigma \cdot \mathcal{L}$
Title=$\Delta R$ between then two leading leptons ($ee$ channel)
LogY=0
#RatioPlotYMax=15
#RatioPlotYMin=-15
## END PLOT

## BEGIN PLOT /ATLAS_2019_llvv/SM_dR_mm
XLabel=$\Delta R_{ll}$
YLabel=$\sigma \cdot \mathcal{L}$
Title=$\Delta R$ between then two leading leptons ($\mu\mu$ channel)
LogY=0
#RatioPlotYMax=15
#RatioPlotYMin=-15
## END PLOT
